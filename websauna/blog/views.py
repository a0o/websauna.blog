from typing import Iterable
import logging

import deform
import colander
import markdown
from pyramid.decorator import reify
from pyramid.security import Allow, Deny
from pyramid.security import Everyone
from pyramid.view import view_config
from websauna.system.core.route import simple_route
from websauna.system.core.views.redirect import redirect_view
from websauna.system.crud.paginator import DefaultPaginator
from zope.interface import implementer
from sqlalchemy import cast, func

from websauna.system.form.resourceregistry import ResourceRegistry
from websauna.system.core.breadcrumbs import get_breadcrumbs
from websauna.system.core.interfaces import IContainer
from websauna.system.core.root import Root
from websauna.system.core.traversal import Resource
from websauna.system.http import Request
from websauna.compat.typing import List
from sqlalchemy import Integer

from bs4 import BeautifulSoup
from allcleanapp.models import BlogPostMeta
from allcleanapp.schemas import PopupGetQuoteSchema
from .models import Post


logger = logging.getLogger(__name__)


class PostResource(Resource):
    """Wrap SQLAlchemy Post model to traversing resource."""

    def __init__(self, request: Request, post: Post):
        super(PostResource, self).__init__(request)
        self.post = post

    def get_title(self) -> str:
        return self.post.title

    def get_body_as_html(self) -> str:
        return markdown.markdown(self.post.body)

    def get_heading_class(self) -> str:
        """Visually separate draft posts from published posts when viewing blog roll as admin."""

        if self.post.published_at:
            return ""
        else:
            return "text-danger"

    @reify
    def __acl__(self) -> List[tuple]:
        """Dynamically give blog post permissions."""

        # Only published posts are viewable to the audience
        if self.post.published_at:
            return [
                (Allow, Everyone, "view"),
            ]
        else:
            # Draft post
            return [
                (Allow, "group:admin", "view"),  # Only show drafts/previews for admin
                (Deny, Everyone, "view"),
            ]


@implementer(IContainer)
class BlogContainer(Resource):
    """Contains all posts, mounted at /blog/."""

    __acl__ = [
        (Allow, "group:admin", "edit"),  # Needed to render admin edit links in main UI
    ]

    def get_title(self):
        title = self.request.registry.settings.get("blog.title", "all clean blog")
        return title

    def wrap_post(self, post: Post) -> "PostResource":
        """Convert raw SQLAlchemy Post instance to traverse and permission aware PostResource with its public URL."""
        res = PostResource(self.request, post)
        return Resource.make_lineage(self, res, post.slug)

    def get_posts(self) -> Iterable[PostResource]:
        """List all posts in this folder.

        We filter out by current user permissions.
        """

        dbsession = self.request.dbsession
        q = dbsession.query(Post).order_by(Post.published_at.desc())

        for post in q:
            resource = self.wrap_post(post)
            if self.request.has_permission("view", resource):
                yield resource

    def popular_posts(self) -> Iterable[PostResource]:
        """List all popular posts in this folder.

        We filter out by current user permissions.
        """

        dbsession = self.request.dbsession
        # Post.other_data["views_count"]
        q = dbsession.query(Post). \
            order_by(Post.other_data["views_count"].desc()). \
            limit(5)

        for post in q:
            souped_post = BeautifulSoup(post.body, 'html.parser')
            image = souped_post.find(id="share_img", src=True)

            if image is None:
                try:
                    thumb_image = (souped_post.find_all('img')[0]).get('src')
                except IndexError as ie:
                    thumb_image = 'logo.jpg'
                except Exception as e:
                    print(e)

                post.thumb_image = thumb_image

            resource = self.wrap_post(post)
            if self.request.has_permission("view", resource):
                yield resource

    def get_popular_posts(self) -> List[PostResource]:
        """Get all blog posts.

        We return a list instead of iterator, so we can test for empty blog condition in templates.
        """
        return list(self.popular_posts())

    def get_posts_by_tag(self, tag: str) -> Iterable[PostResource]:
        """Lists all posts by a tag within the permissions of a current user."""
        for resource in self.get_posts():
            if tag in resource.post.get_tag_list():
                yield resource

    def items(self):
        """Sitemap support."""
        for resource in self.get_posts():
            yield resource.__name__, resource

    def get_roll_posts(self) -> List[PostResource]:
        """Get all blog posts.

        We return a list instead of iterator, so we can test for empty blog condition in templates.
        """
        return list(self.get_posts())

    def __getitem__(self, item: str) -> PostResource:
        """Traversing to blog post."""

        dbsession = self.request.dbsession
        item = dbsession.query(Post).filter_by(slug=item).one_or_none()
        if item:
            return self.wrap_post(item)

        raise KeyError()



def blog_container_factory(request) -> BlogContainer:
    """Set up __parent__ and __name__ pointers for BlogContainer required for traversal."""
    folder = BlogContainer(request)
    root = Root.root_factory(request)
    return Resource.make_lineage(root, folder, "blog")


@view_config(route_name="blog", context=BlogContainer, name="", renderer="allcleanapp:templates/blog/blog_roll.html")
def blog_roll(blog_container, request):
    """Blog index view."""
    breadcrumbs = get_breadcrumbs(blog_container, request)

    # Get a hold to admin object so we can jump there
    post_admin = request.admin["models"]["blog-posts"]
    paginator = DefaultPaginator(default_size=5)
    blog_posts = list(blog_container.get_roll_posts())
    popular_posts = blog_container.get_popular_posts()
    count = len(blog_posts)
    batch = paginator.paginate(blog_posts, request, count)

    # from pprint import pprint
    # for post in blog_container.get_popular_posts():
    #     pprint(post.post.__dict__, indent=2)

    return locals()


@view_config(route_name="blog_tag", renderer="blog/tag_roll.html")
def tag(blog_container: BlogContainer, request: Request):
    """Tag roll."""

    tag = request.matchdict["tag"]
    current_view_url = request.url
    current_view_name = "Posts tagged {}".format(tag)
    breadcrumbs = get_breadcrumbs(blog_container, request, current_view_name=current_view_name, current_view_url=current_view_url)

    # wrap to list() to handle empty result correctly
    tagged_posts = list(blog_container.get_posts_by_tag(tag))

    # Get a hold to admin object so we can jump there
    post_admin = request.admin["models"]["blog-posts"]
    paginator = DefaultPaginator(default_size=5)
    tagged_posts = list(blog_container.get_posts_by_tag(tag))
    count = len(tagged_posts)
    batch = paginator.paginate(tagged_posts, request, count)

    return locals()


def get_single_pop_posts(request: Request):
    dbsession = request.dbsession
    q = dbsession.query(Post). \
        order_by(Post.other_data["views_count"].desc()). \
        limit(5)

    for post in q:
        souped_post = BeautifulSoup(post.body, 'html.parser')
        image = souped_post.find(id="share_img", src=True)

        if image is None:
            try:
                thumb_image = (souped_post.find_all('img')[0]).get('src')
            except IndexError as ie:
                thumb_image = 'logo.jpg'
            except Exception as e:
                print(e)
            post.thumb_image = thumb_image

        yield post

@view_config(route_name="blog", context=PostResource, name="", renderer="allcleanapp:templates/blog/post.html")
def blog_post(post_resource, request):
    """Single blog post."""
    breadcrumbs = get_breadcrumbs(post_resource, request)
    post = post_resource.post
    disqus_id = request.registry.settings.get("blog.disqus_id", "").strip()

    last_count = 0
    other_data = {}
    try:
        last_count = post.other_data['views_count']
    except KeyError as e:
        other_data = {
            "views_count": last_count
        }
        other_data.update(post.other_data)

    other_data['views_count'] = last_count + 1
    extra_dump = {}
    other_data.update(extra_dump)
    dbsession = request.dbsession
    post_upd = dbsession.merge(Post(id=post.id))
    post_upd.other_data = other_data
    dbsession.add(post_upd)
    dbsession.flush()

    single_pop_posts = list(get_single_pop_posts(request))

    # Add form
    popup_schema = PopupGetQuoteSchema().bind(request=request)
    b = deform.Button(name='add', title="Submit", css_class="button-secondary btn-lg")
    c = deform.Button(name='cancel', title="Cancel", css_class="button-secondary btn-lg")
    form = deform.Form(popup_schema, buttons=(b,c), resource_registry=ResourceRegistry(request), use_ajax=True)
    form.formid = "addevent-popup"
    rendered_form = form.render()
    form.resource_registry.pull_in_resources(request, form)

    souped_post = BeautifulSoup(post_resource.get_body_as_html(), 'html.parser')
    image = souped_post.find(id="share_img", src=True)

    if image is None:
        try:
            page_facebook_image = (souped_post.find_all('img')[0]).get('src')
        except IndexError as ie:
            page_facebook_image = None
    else:
        page_facebook_image = image['src']
        extracted_width = image['width']
        extracted_height = image['height']

    try:
        from urllib.request import urlopen
        import io
        from PIL import Image
        file = io.BytesIO(urlopen(page_facebook_image).read())
        im=Image.open(file)
        img_width, img_height = im.size

        page_facebook_image_width = img_width if img_width > 199 else 200
        page_facebook_image_height = img_height if img_height > 199 else 200
    except Exception as e:
        pass

    # print('JJJ'*100)
    # post_meta = BlogPostMeta()
    # post_meta.blog_post_id = post.uuid
    # post_meta.views_count += 1
    # request.dbsession.add(post_meta)
    # request.dbsession.flush()

    return locals()


def get_post_resource(request: Request, slug: str) -> PostResource:
    """Helper function to get easily URL mapped blog posts."""

    container = blog_container_factory(request)
    try:
        return container[slug]
    except KeyError:
        return None

# Convenience redirect /blog -> /blog/
_redirect = redirect_view("/blog", new_path="/blog/", status_code=302)
